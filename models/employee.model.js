const mongoose = require('mongoose');

var employeeSchema = new mongoose.Schema({
    ncuenta: {
        type: Number,
        required: 'Este campo es requerido.'
    },
    nombre: {
        type: String,
        require: "Este campo es requerido"
    },
    tipocuenta: {
        type: String,
        require: "Este campo es requerido"
    },
    saldo: {
        type: Number,
        require: "Este campo es requerido"
    },
    valorDeposito: {
        type: Number,
        require: "Este campo es requerido"
    },
    valorRetiro: {
        type: Number,
        require: "Este campo es requerido"
    }
});

// Custom validation for email
/*employeeSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');*/

mongoose.model('Employee', employeeSchema);